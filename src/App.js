import React, { Component } from 'react';
import logo from './logo.svg';
import './App.css';

class App extends Component  {
  state = {
    races: []
  }

  componentDidMount() {
    fetch('next-to-jump.json')
    .then(res => res.json())
    .then((data) => {
      this.setState({ races: data.result })
    })
    .catch(console.log)
  }

  getTimeStr(dt){
    var delta = Math.abs(new Date(dt)-new Date()) / 1000
    var days = Math.floor(delta / 86400)

    delta -= days * 86400
    var hours = Math.floor(delta / 3600) % 24
    
    delta -= hours * 3600
    var minutes = Math.floor(delta / 60) % 60
    
    delta -= minutes * 60
    var seconds = Math.floor(delta % 60)

    return `${days} days ${hours} hours ${minutes} mins ${seconds} secs`
  }

  render() {

    return (
       <div className="container">
        <div className="col-md-8">
        <h1>Next 5 Races</h1>
        {this.state.races.map((race) => (
          <div className="card">
            <div className="card-body">
              <div class="pull-left">
                <h5 className="card-title">{race.Venue.Venue}</h5>
                <span className="card-title">{race.EventName}</span>
                <div class="float-right">
                  <span>{this.getTimeStr(race.AdvertisedStartTime)}</span>
                </div>
              </div>              
            </div>
          </div>
        ))}
        </div>
       </div>
    );
  }
}

export default App;